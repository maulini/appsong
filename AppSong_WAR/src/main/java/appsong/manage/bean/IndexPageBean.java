package appsong.manage.bean;

import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.inject.spi.CDI;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import appsong.entities.Song;
import appsong.facade.FacadeSong;
import lombok.AccessLevel;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import java.io.Serializable;

@SuppressWarnings("serial")
@Named
@ViewScoped

@FieldDefaults(level=AccessLevel.PRIVATE)
public class IndexPageBean implements Serializable {

	@Inject
	FacadeSong metierSong;
	
	@Inject
	@Setter
	Song song;
	
	public void createSong() {
		if(!metierSong.containSong(song)) {
			metierSong.createSong(song);
			song = CDI.current().select(Song.class).get();
		}else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Alredy Exsist", String.format("The song %s alredy exist in the BDD", song.getName())));
		}
	}
	
	public void removeSong(Song s) {
		metierSong.removeSong(s);
	}
	
	public List<Song> readAll(){
		return metierSong.readAllSong().collect(Collectors.toList());
	}
	
}
