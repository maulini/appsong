package appsong.facade;

import java.util.stream.Stream;

import javax.ejb.Stateless;
import javax.inject.Inject;

import appsong.dao.DaoSong;
import appsong.entities.Song;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level=AccessLevel.PRIVATE)
@Stateless
public class FacadeSong {

	@Inject
	DaoSong persistanceSong;
	
	public void createSong(Song song) {
		persistanceSong.create(song);
	}
	
	public void updateSong(Song song) {
		persistanceSong.update(song);
	}
	
	public void removeSong(Song song) {
		persistanceSong.remove(song);
	}
	
	public boolean containSong(Song song) {
		return persistanceSong.contains(song);
	}
	
	public Stream<Song> readAllSong() {
		return persistanceSong.readAll();
	}
	
	public Song readSong(String id) {
		return persistanceSong.read(id);
	}
	
}
