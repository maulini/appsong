package appsong.entities;

import java.io.File;

import javax.enterprise.context.Dependent;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

//LBK
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=false)
@FieldDefaults(level=AccessLevel.PRIVATE)

//JPA
@Entity
@Table(name="SONG")
@NamedQueries({
	@NamedQuery(name="song.readAll", query="SELECT s FROM Song s")
})

//CDI
@Dependent
public class Song extends AEntity {

	String name;
	
	String autor;
	
	File file;
	
	@Lob
	byte[] image;
	
}
