package appsong.entities;

import java.io.Serializable;
import java.util.UUID;

import javax.inject.Inject;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

@SuppressWarnings("serial")
@FieldDefaults(level=AccessLevel.PRIVATE)
@MappedSuperclass
@EqualsAndHashCode(of="id")
public abstract class AEntity implements Serializable {

	@Id
	@Column(columnDefinition="VARCHAR(36)")
	@Inject
	UUID id;
	
}
