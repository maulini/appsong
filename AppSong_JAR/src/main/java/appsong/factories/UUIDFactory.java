package appsong.factories;

import java.io.Serializable;
import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

@SuppressWarnings("serial")
@ApplicationScoped
public class UUIDFactory implements Serializable{

	@Produces
	public UUID uuidFactory() {
		return UUID.randomUUID();
	}
	
}
