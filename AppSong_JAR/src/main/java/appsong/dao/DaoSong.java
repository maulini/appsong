package appsong.dao;

import java.io.Serializable;
import java.util.stream.Stream;

import javax.enterprise.context.Dependent;

import appsong.entities.Song;

@SuppressWarnings("serial")
@Dependent
public class DaoSong extends ADao<Song> implements Serializable {

	@Override
	public Stream<Song> readAll() {
		return super.persistance.createNamedQuery("song.readAll", Song.class).getResultStream();
	}

}
