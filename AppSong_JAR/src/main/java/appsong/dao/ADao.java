package appsong.dao;

import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level=AccessLevel.PROTECTED)
public abstract class ADao<T> {

	@PersistenceContext
	EntityManager persistance;
	
	Class<T> type;
	
	public void create(T object) {
		persistance.getTransaction().begin();
		persistance.persist(object);
		persistance.getTransaction().commit();
	}
	
	public void remove(T object) {
		persistance.getTransaction().begin();
		persistance.remove(object);
		persistance.getTransaction().commit();
	}
	
	public void update(T object) {
		persistance.getTransaction().begin();
		persistance.merge(object);
		persistance.getTransaction().commit();
	}
	
	public boolean contains(T object) {
		return persistance.contains(object);
	}

	public T read(String id) {
		return (T) persistance.find(type, id);
	}
	
	public abstract Stream<T> readAll();
}
